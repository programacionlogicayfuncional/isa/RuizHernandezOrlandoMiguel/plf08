## Cifrado por transpocisión
### Permutaciones por series
### Descripcion del proyecto:

- Es una aplicacion  clojure que toma como parametro de entrada la operacion a realizar: cifrado/descifrado
  el archivo de texto plano de entrada que será procesado y el archivo de texto plano de salida donde será escrito
  el texto procesado.   

### Cifrado
1. Del texto leido del archivo .txt guarda las posiciones inpares de los caracteres.
2. Del texto leido del archivo .txt guarda las posiciones pares de los caracteres
3. concatena ambas cadenas colocando primero las posiciones pares y seguidamente las inpares.
4. Esta nueva cadena la escribe en el archivo txt de salida.

### Descifrado
1. Del texto leido del archivo .txt guarda las posiciones inpares de los caracteres.
2. Del texto leido del archivo .txt guarda las posiciones pares de los caracteres
3. concatena ambas cadenas colocando primero las posiciones pares y seguidamente las inpares.
4. Esta nueva cadena la escribe en el archivo txt de salida.

## Forma de ejecutar:

1. Abrir la consola de comandos
2. Entrar dentro de la carpeta del proyecto
3. Introducir el código de operacion:
    1 : si se quiere codificar un texto
    2 : si se quiere descodificar un texto
   seguidamente se agrega la direccion del texto a cifrar: "C:\\ archivo_entrada.txt"
   finalmente se agrega la direccion del archivo donde será escrito: "C:\\ archivo_salida.txt"

## Opciones para su ejecución.

1. ingresar 1 codifica el texto
2. ingresar 2 descodifica el exto

## Ejemplos de uso o ejecución.



## Errores, limitantes, situaciones no consideradas o particularidades de tu solución.

1. Durante el desarrollo del proyecto no sé contempló que la transposicion se realizará
   por medio de la transposición columnar simple.
2. Al no ser diseñado para una transposición columnar simple no requiere el parametro de la plabra clave como entrada 