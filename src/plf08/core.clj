(ns plf08.core
  (:gen-class))
; ######################## CODIFICADOR TRANSPOSICION ##############################33

; ###########  CODIFICDOR  ###########

(defn codificar-transposicion
  [x]
  (str (apply str (keep-indexed #(if (odd? %1) %2) (map str x)))
       (apply str (keep-indexed #(if (even? %1) %2) (map str x)))
  ))

; ###########  DECODIFICDOR  ###########
(defn decodificador-transposicion-inpar
  [x]
  (apply str (interleave
              (apply str (keep-indexed #(if (< (- (/ (count x) 2) 1) %1) %2) (map str x)))
              (str (apply str (keep-indexed #(if (> (- (/ (count x) 2) 1) %1) %2) (map str x))) " "))
  ))

(defn decodificador-transposicion-par
  [x]
  (apply str (interleave
              (apply str (keep-indexed #(if (<= (/ (count x) 2) %1) %2) (map str x)))
              (str (apply str (keep-indexed #(if (> (/ (count x) 2) %1) %2) (map str x)))))
  ))



(defn decodificar-transposicion
  [x]
  (cond
    (even? (count x)) (decodificador-transposicion-par x)
    :else (decodificador-transposicion-inpar x)))


; ###########  MAIN  ###########

(defn -main
  [opcion salida entrada]
  (cond (= opcion 1) (spit salida (codificar-transposicion (slurp entrada)))
        (= opcion 2) (spit salida (decodificar-transposicion (slurp entrada)))
        :else  "Opcion no valida"))
 
;(defn -main 
;  [entrada salida]
;  (spit salida (slurp entrada)))

(-main 1 "D:\\salida.txt" "D:\\entrada.txt")
(-main 2 "D:\\salida2.txt" "D:\\salida.txt")
